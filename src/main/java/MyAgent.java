import java.lang.instrument.Instrumentation;

public class MyAgent {

    private static Instrumentation instrumentation;

    public static void agentmain(String args, Instrumentation inst) throws Exception {
        instrumentation = inst;
        instrumentation.addTransformer(new JavassistSimpleTransformer());
        instrumentation.addTransformer(new ASMSimpleTransformer());
    }

    public static void initialize() {
        if (instrumentation == null) {
            MyJavaAgentLoader.loadAgent();
        }
    }

}
