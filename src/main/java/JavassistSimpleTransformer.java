import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

public class JavassistSimpleTransformer implements ClassFileTransformer {

    public byte[] transform(ClassLoader loader, String className,
                            Class<?> classBeingRedefined, ProtectionDomain protectionDomain,
                            byte[] bytes) throws IllegalClassFormatException {

        byte[] result = bytes;

        if (className.contains("AssistTest1")) {
            try {
                String dotClassName = className.replace('/', '.');
                ClassPool cp = ClassPool.getDefault();
                CtClass ctClazz = cp.get(dotClassName);
                ctClazz.defrost();
                CtMethod method1 = ctClazz.getDeclaredMethod("doSomething");
                method1.insertBefore("System.out.println(\"Just called doSomething method!\");");
                result = ctClazz.toBytecode();
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
        return result;
    }

}
