import java.net.URL;
import java.net.URLClassLoader;

public class Main {

    private static final int NUMBER_OF_CLASSES_TO_GENERATE = 5000;

    public static void main(String[] args) throws Exception {
        MyAgent.initialize();

        double AssistResult = conductExperiment(AssistTest1.class);
        double ASMResult = conductExperiment(AssistTest2.class);
        System.out.println("Czas generowania klas z uzyiem javassist: " + AssistResult);
        System.out.println("Czas generowania klas z uzyciem ASM: " + ASMResult);
        if(ASMResult < AssistResult && AssistResult != 0) {
            double perc = Math.round((AssistResult - ASMResult)/AssistResult * 10000);
            perc = perc/100;
            System.out.println("ASM szybszy od Assist o " + perc + "%");
        } else if (AssistResult < ASMResult && ASMResult != 0) {
            double perc = Math.round((ASMResult - AssistResult)/ASMResult * 10000);
            perc = perc/100;
            System.out.println("Assist szybszy od ASM o " + perc + "%");
        }
        System.out.println("\nPrezentacja dzialania zmienionych metod: ");
        System.out.println("\nPrzez javassist:");
        AssistTest1 assistTest1 = new AssistTest1();
        assistTest1.doSomething();
        System.out.println("\nPrzez ASM:");
        AssistTest2 assistTest2 = new AssistTest2();
        assistTest2.doSomething();
    }

    private static void loadClasses(URL[] urls, ClassLoader delegateParent, String className) {
        for (int i = 0; i < NUMBER_OF_CLASSES_TO_GENERATE; i++) {
            try {
                ClassLoader cl = new URLClassLoader(urls, delegateParent);
                cl.loadClass(className);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static long conductExperiment(Class<?> cls) {
        long startTime = System.currentTimeMillis();
        URL[] urls = {cls.getProtectionDomain().getCodeSource().getLocation()};
        ClassLoader delegateParent = cls.getClassLoader().getParent();
        loadClasses(urls, delegateParent, cls.getName());
        long finishTime = System.currentTimeMillis();
        return finishTime - startTime;
    }
}
