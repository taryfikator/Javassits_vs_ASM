### Autor: Magdalena Tokarczyk ###

## Javassist vs. ASM

Napisałam aplikację, która ładuje po 5000 razy dwie klasy różniące się jedynie nazwą.
Podczas ładowania jednej z nich Javassist zmienia metodę doSomething(), dodając wypisanie "Just called doSomething method!".
Podczas ładowania drugiej taką samą operację przeprowadza ASM.
Czas ładowania tych klas jest mierzony i wypisywany. Do tego wypisywane jest o ile procent szybsze jest jedno narzędzie od drugiego.

Dodatkowo prezentowane jest działanie obu zmienionych metod.

Uruchomienie programu:
Komenda: mvn install exec:java